""" Probability graph API module """
import json
import math
import operator
import os
import pickle
from flask import Flask
from flask_restful import Resource, Api, reqparse
from xpresso.ai.core.logging.xpr_log import XprLogger

APP = Flask(__name__)
API = Api(APP)


# Model loading function
def load_model(filename):
    """ Loading graph """
    input_dict = open(filename, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


# Getting model data
def load_test_file_helper():
    """ helper function for test sequence generation """
    edges = {}
    for item in RANGE_LIST:
        MY_LOGGER.info("Loading file %s", str(item))
        print("Loading file", str(item))
        filename = PG_SPLIT.replace('VAL', str(item))
        edges.update(load_model(filename))
    return edges


# Getting final prediction
def get_model_output(test_seq, number):
    """ Get sequence and return the predicted sequence """
    test_seq_final = test_seq[-LOOKAHEAD:]
    weight_predict = WEIGHT[1 - len(test_seq_final):] + [1]
    predict_dict = {}
    for ind, seq in enumerate(test_seq_final):
        if seq in MODELDATA:
            for block, prob in MODELDATA[seq][:number]:
                predict_dict[block] = round(prob * weight_predict[ind], 5)
    predict_dict_sorted = sorted(predict_dict.items(), key=operator.itemgetter(1), reverse=True)
    predicted = [item[0] for item in predict_dict_sorted][:number]
    return predicted


# Logger untility function
def create_logger():
    """ Creating logger """
    path = os.getcwd()
    config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
    my_logger = XprLogger(config_path = config_file)
    return my_logger


# Probability graph class for GUI
class PGraphPredict(Resource):
    """ Declaring and defining class objects """

    def post(self):
        """ Method for POST response """
        try:
            PARSER.add_argument('Topk', type=str)
            PARSER.add_argument('InputSequence', type=str)
            args = PARSER.parse_args()
            block = args['InputSequence'].split(' ')
            neighbor = int(args['Topk'])
            MY_LOGGER.info('Input %s', block)
            MY_LOGGER.info(f'TopK {neighbor}')
            print(args)
            final_output = get_model_output(block, neighbor)
            MY_LOGGER.info(f'Output {final_output}')
            print(f'Block_Sequence: {final_output}')
            return {'Block_Sequence': final_output}, 200
        except Exception:
            final_output = "Internal Server Error"
            MY_LOGGER.info('Output %s', final_output)
            return final_output, 500


PARSER = reqparse.RequestParser()
API.add_resource(PGraphPredict, '/pg_predict_block')

if __name__ == '__main__':
    # Reading config data from json
    CONFIG_PATH = 'config/config.json'
    CONFIG_FILE = open(CONFIG_PATH, 'r')
    JSON_OBJECT = json.load(CONFIG_FILE)
    CONFIG_FILE.close()

    DATA_MODEL = JSON_OBJECT['model']['pg_bn']
    DATA_PREP_PARAM = JSON_OBJECT['data_preprocess']

    INPUTFILE = DATA_PREP_PARAM['input_file']

    READ_WRITE_FLAG = DATA_PREP_PARAM['use_read_write_operation_feature']
    SPLITPERC = DATA_PREP_PARAM['train_split_ratio']
    CUTTING_WINDOW = DATA_PREP_PARAM['cutting_window']

    FILENAME = str(INPUTFILE.split('/')[-1].split('.')[0])
    MASTERPATH = DATA_PREP_PARAM['master_path']
    MASTERPATH = MASTERPATH.replace("FILE", FILENAME)
    MASTERPATH = MASTERPATH.replace("CUTTING", str(CUTTING_WINDOW))
    MASTERPATH = MASTERPATH.replace("SPLIT", str(SPLITPERC))
    MASTERPATH = MASTERPATH.replace("RW", str(READ_WRITE_FLAG))
    MOUNTEDPATH = DATA_PREP_PARAM['mounted_path'] + MASTERPATH
    MODELPATH = MOUNTEDPATH + DATA_MODEL['model_path_pg']

    LOGPATH = MODELPATH + DATA_PREP_PARAM['logfile_inference']
    LOOKAHEAD = DATA_MODEL['lookahead_window']
    WEIGHT = [round(math.exp(val * -0.65), 5) for val in range(LOOKAHEAD - 1, 0, -1)]
    RANGE_LIST = range(1, LOOKAHEAD + 1)
    PG_SPLIT = MODELPATH + DATA_MODEL['output_files']['pg_split']

    MY_LOGGER = create_logger()

    MY_LOGGER.info("Process started")
    print("Process started")

    MY_LOGGER.info("Model loading... ")
    print("Model loading... ")
    MODELDATA = load_test_file_helper()
    MY_LOGGER.info("Model loading Completed")
    print("Model loading Completed")

    HOSTNAME = DATA_MODEL['hostname']
    PORT = DATA_MODEL['pg_api_port']
    APP.run(host=HOSTNAME, port=PORT)
